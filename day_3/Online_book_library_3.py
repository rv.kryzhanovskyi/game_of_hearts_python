from typing import Optional


class Author:
    def __init__(
        self, first_name: str, last_name: str, year_of_birth: Optional[int] = None
    ) -> None:
        """"""
        self.first_name = first_name
        self.last_name = last_name
        self.year_of_birth = year_of_birth

    def __str__(self) -> str:
        """"""
        return f"{self.first_name} {self.last_name} {self.year_of_birth}"

    def __repr__(self) -> str:
        """"""
        return (
            f"Author('{self.first_name}', '{self.last_name}', '{self.year_of_birth}')"
        )

    def __eq__(self, other: "Author") -> bool:
        """"""
        if not isinstance(other, Author):
            raise TypeError(
                f"for type _____ and type______operation is not implemented"
            )
        return (
            self.first_name == other.first_name
            and self.last_name == other.last_name
            and self.year_of_birth == other.year_of_birth
        )

    def __hash__(self) -> int:
        """"""
        return hash((self.first_name, self.last_name, self.year_of_birth))


class Genre:
    def __init__(
        self, genre_name: str, description_of_genre: Optional[str] = None
    ) -> None:
        """"""
        self.genre_name = genre_name
        self.description_of_genre = description_of_genre

    def __str__(self) -> str:
        """"""
        return self.genre_name

    def __repr__(self) -> str:
        """"""
        return f"Genre('{self.genre_name}', '{self.description_of_genre}')"


class Book:
    def __init__(
        self,
        authors: Author,
        name: str,
        language: str,
        year_of_publication: int,
        genres: Genre,
        isbn: str,
        description: Optional[str],
    ) -> None:
        """"""
        self.authors = authors
        self.name = name
        self.language = language
        self.year_of_publication = year_of_publication
        self.genres = genres
        self.isbn = isbn
        self.description = description

    def __str__(self) -> str:
        """"""
        return (
            f"Автор: '{self.authors}', "
            f"Книга: '{self.name}', "
            f"Мова: '{self.language}', "
            f"Рік видання: '{self.year_of_publication}', "
            f"Жанр: '{self.genres}', "
            f"ISBN: '{self.isbn}', "
            f"Опис: '{self.description}'"
        )

    def __repr__(self) -> str:
        """"""
        return (
            f"Book('{self.authors}', "
            f"'{self.name}', "
            f"'{self.language}', "
            f"'{self.year_of_publication}', "
            f"'{self.genres}' "
            f"'{self.isbn}', "
            f"'{self.description}'"
        )

    def __eq__(self, other: "Book") -> bool:
        """"""
        if not isinstance(other, Book):
            raise TypeError(
                f"for type _____ and type______operation is not implemented"
            )
        return set(self.name) == set(other.name) and self.authors == other.authors


author_1 = Author("Franklin", "Herbert", 1920)
genre_1 = Genre("fantasy")
book = Book(
    author_1,
    "Dune",
    "English",
    2005,
    genre_1,
    "978-0-450-01184-9",
    "The epic story of the planet Arrakis, its Atreides rulers and their mortal enemies the Harkonnens\
                is the finest, most widely acclaimed and enduring science fiction novel of this century.",
)

print(book)
