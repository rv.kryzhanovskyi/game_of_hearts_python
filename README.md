Даний репозиторій создано для відпрацювання практичних навичок програмування на мові __Python__, під час проходження Марафону «Пишемо гру Hearts на Python».

---

### Зміст репозиторію:

- Папка "day_1" - інформація, щодо рішення тренувальних задач,
- Папка "hometask" - інформація, щодо домашніх завдань,
- Папка "Script" - код карткової гри HEARTS.
- ...

---

#### Task_1 (_підрахунок ймовірності зустріти символ в рядку_)

Написати функцію яка:

- приймає рядок і повертає словник, у якому ключами є всі символи, які зустрічаються в цьому рядку, а значення - відповідні
вірогідності зустріти цей символ в цьому рядку, 
- код повинен бути структурований за допомогою конструкції "if \_\_name__ == "\_\_main__": ...",
- всі аргументи та значення що функція повертає, повинні бути типізовані, функція має рядок документації.

#### Приклад:
...додати приклад використання створених функцій (як вставки з інтерактивної консолі)

#### Task_2 (_використання тернарного оператора_)

Написати функцію, яка за допомогою тернарного оператора реалізує наступну логіку:

- є параметри x та у,
- якщо x < y - друкуємо x + y,
- якщо x == y - друкуємо 0,
- якщо x > y - друкуємо x - y,
- якщо x == 0 та y == 0 друкуємо "game over"

#### Приклад:
...додати приклад використання створених функцій (як вставки з інтерактивної консолі)

---

### Game_of_Hearts_Python

#### Правила гри, коротко:

Грають четверо гравців, ім роздається французька колода - по 13 карт у кожного.

Гравець, який тримає ♣2, починає перший раунд і повинен починати саме з ♣2.

Гравці по черзі грають свою карту, дотримуючись першої масті, якщо це можливо.

Гравець, який розігрує старшу карту головної масті, збирає "взятку" і стає початковим гравцем у наступному ході.

Гравець не може класти карту з ♡, доки ♡ не буде зіграно в попередньому прийомі (тобто - вперше ♡ може бути розіграна лише якщо у гравця не має масті, з якої почався круг).

Коли всі карти розіграно, гравці отримують очки, якщо вони беруть певні карти:
- 13 балів за ♠Q
- 1 бал за кожне ♡

Гра триває кілька раундів, поки один гравець не набере 100 очок або більше. Перемагає гравець з найменшою кількістю балів


### _Власник репозиторію:_
Крижановський Роман

rv.kryzhanovskyi@gmail.com
