# Написати функцію якам приймає рядок і повертає словник у якому
# ключами є всі символи, які зустрічаються в цьому рядку, а значення - відповідні
# вірогідності зустріти цей символ в цьому рядку.
# № код повинен бути структурований за допомогою конструкції if __name__ == "__main__":,
# всі аргументи та значення що функція повертає повинні бути типізовані, функція має рядок документації

from collections import Counter
from typing import Dict


def get_symbols_frequency_with_counter(text: str) -> Dict[str, float]:
    """
    Функція перетворює рядок в словник, де ключ це символ рядка,
    а значення - йомовірність знаходження цого символу у рядку,
    за допомогою модуля Counter
    """
    texts_len = len(text)
    return {key: value / texts_len for key, value in Counter(text).items()}


def get_symbols_frequency(text: str) -> Dict[str, float]:
    """
    Функція перетворює рядок в словник, де ключ це символ рядка,
    а значення - йомовірність знаходження цого символу у рядку,
    за допомогою перетворення рядка в множину та подальшої ітерації
    """
    result_dict = {}
    tests_len = len(text)
    for symbol in set(text):
        result_dict[symbol] = text.count(symbol) / tests_len
    return result_dict


if __name__ == "__main__":
    variation = (("1111", {"1": 1.0}), ("1", {"1": 1.0}), ("ab", {"a": 0.5, "b": 0.5}))
    for string, result in variation:
        result_func = get_symbols_frequency(string)
        assert (
            result_func == result
        ), f"ERROR: symbols_frequency({string}) returned {result_func}, but expected: {result}"

    print(get_symbols_frequency_with_counter("ab"))
